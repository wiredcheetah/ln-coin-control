# LN-coin-control

coin control for c-lightning

dependencies: https://github.com/ElementsProject/lightning

requires a running c-lightning node. it will shut the node down to less dangerously manipulate the database.

wired cheetah does not know for sure if it is safe.

the python scripts should not be run without shutting down the LN node first. wired cheetah leaves the bitcoin node running.