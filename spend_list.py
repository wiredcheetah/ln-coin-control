#!/usr/bin/env python2.7

def main():
    import sys
    import json
    import sqlite3
    from os.path import expanduser
    home = expanduser("~")
    dbpath=home+"/.lightning/lightningd.sqlite3"
    print("enter addresses from which to spend, separated by spaces, and hit enter. leave the list empty to freeze all.")
    addrs = sys.stdin.readline()
    with open('addr_list') as f:
        data = json.load(f)
    addresses = addrs.split()
    counter=0
    conn = sqlite3.connect(dbpath)
    c = conn.cursor()
    c.execute('''SELECT * FROM outputs''')
    query='''UPDATE outputs SET status=128 WHERE status=0'''
    c.execute(query)
    for index in data['addresses']:
        dump=json.dumps(index)
        for address in addresses:
            if address in dump and len(address)>31:
                print str(counter) + " "
                query='''UPDATE outputs SET status=0 WHERE keyindex='''+str(counter)+''' AND status=128'''
                c.execute(query)
        counter+=1
    conn.commit()

if __name__ == '__main__':
     main()
